'use strict';

var express = require('express');
var app = express();
var listenPort = 8080;

app.use('/', express.static(__dirname + '/public'));

app.all('/', function(req, res) {
    res.sendFile('/public/index.html', { root: __dirname });
});

app.listen(8080, function () {
    console.log('Server listen port: ' + listenPort);
});