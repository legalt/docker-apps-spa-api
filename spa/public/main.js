function http ( url, config ) {
	var xhr = new XMLHttpRequest(),
		promise = {
          success: function () {},
          error: function () {}
        };
        
    config = config || {};

	config.async = config.async || true;
	config.method = config.method || 'GET';
	config.data = config.data || '';
  
	url = url || '/';

	xhr.open(config.method, url, config.async);
	xhr.send(config.data);

	if ( config.async ) {
		xhr.onreadystatechange = function() { // (3)
		  	if (xhr.readyState != 4) return;

		  	if ( xhr.status !== 200 ) {
				promise.error({
					status: xhr.status,
					error: xhr.statusText
				});
                
			} else {
				promise.success(JSON.parse(xhr.responseText));
			}
		}
	} else {
		if ( xhr.status !== 200 ) {
			promise.error({
				status: xhr.status,
				error: xhr.statusText
			});
		} else {
			promise.success(JSON.parse(xhr.responseText));
		}
	}

	return {
		then: function ( success, error ) {
			promise.success = success;
            promise.error = error;
		}
	};
}

http('http://localhost:8000/todos', {}).then(function( response ) {
	var $list = window.todoList,
		$item, $indexSpan, $titleSpan, $statusDiv, index;

		for ( index = 0; index < response.length; index++ ) {
			$item = document.createElement('li');

			$item.className = response[index].completed ? 'completed' : '';

			$indexSpan = document.createElement('span');
			$indexSpan.className = 'index';
			$indexSpan.innerText = response[index].id;

			$titleSpan = document.createElement('span');
			$titleSpan.innerText = response[index].title;


			$item.appendChild($indexSpan);
			$item.appendChild($titleSpan);

			$list.appendChild($item);
		}
}, function( response ) {
  // console.error(response);
})