var restify = require('restify'),
	faker = require('faker'),
	_ = require('lodash');

var server = restify.createServer();

server.use(restify.CORS());

server.get('/todos', function ( request, response ) {
	response.send(_.times(30, function ( index ) {
		return {
			id: (index + 1),
			title: faker.lorem.sentence(),
			completed: (index % 2) ? true : false
		};
	}));
});

server.listen(8000, function() {
  console.log('%s listening at %s', server.name, server.url);
});